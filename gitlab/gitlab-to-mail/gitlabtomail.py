#!/usr/bin/env -S python3 -B

from urllib.parse import urljoin, urlparse

import db
import re
import sys
import datetime
import dateutil.parser
import time
import requests
import mailbox
import smtplib
import email
from util import fetch_all, Settings
from assign import Assign

settings = Settings(sys.argv[1])


GITLAB_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

# Compute host name
url = urlparse(settings.GITLAB_URL)
gitlab_hostname = None
if url and url.hostname:
    gitlab_hostname = url.hostname

# BRIDGE TAG HANDLING

if settings.BRIDGE_TAG:
    BRIDGE_TAG_FULL = f"[{settings.BRIDGE_TAG}] "
    BRIDGE_TAG_INFIX = f" {settings.BRIDGE_TAG}"
else:
    BRIDGE_TAG_FULL = ""
    BRIDGE_TAG_INFIX = ""


def log(msg):
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S - ') + msg)


# SEND

def send_email(message):
    del message['Reply-To']
    message['Reply-To'] = settings.BRIDGE_FROM_EMAIL
    message['To'] = settings.BRIDGE_TO_EMAIL

    message['Content-Type'] = 'text/plain; charset=utf-8'
    encoded_message = message.as_string().encode('utf-8')

    if settings.DEBUG:
        mbox = mailbox.mbox("mail_dump")
        mbox.add(encoded_message)
        mbox.close()
        return

    with smtplib.SMTP(settings.SMTP_DOMAIN, settings.SMTP_PORT) as smtp:
        smtp.starttls()
        if settings.SMTP_USER is not None and settings.SMTP_PASSWORD is not None:
            smtp.login(settings.SMTP_USER, settings.SMTP_PASSWORD)
        smtp.sendmail(message['From'], message['To'], encoded_message)


def create_headers_from_event(message, event):
    message['X-GitLab-Project-Id'] = str(event['project_id'])
    if 'note' in event:
        message['X-GitLab-MergeRequest-IID'] = str(event['note']['noteable_iid'])
        mr = fetch_specific_mr(event['note']['noteable_iid'])
        message['X-GitLab-MergeRequest-ID'] = str(mr['id'])
        message['X-GitLab-MergeRequest-NoteID'] = str(event['note']['id'])
    elif 'target_id' in event:
        message['X-GitLab-MergeRequest-IID'] = str(event['target_iid'])
        message['X-GitLab-MergeRequest-ID'] = str(event['target_id'])
    project = fetch_project(event['project_id'])
    message['X-GitLab-Project'] = project['name']
    message['X-GitLab-Project-Path'] = project['path_with_namespace']


def create_headers_from_mr(message, mr):
    message['X-GitLab-Project-Id'] = str(mr['target_project_id'])
    message['X-GitLab-MergeRequest-IID'] = str(mr['iid'])
    message['X-GitLab-MergeRequest-ID'] = str(mr['id'])
    project = fetch_project(mr['target_project_id'])
    message['X-GitLab-Project'] = project['name']
    message['X-GitLab-Project-Path'] = project['path_with_namespace']

# FETCH


def fetch_events(after):
    after = after.strftime("%Y-%m-%d")
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/events?sort=asc&after=f{after}")
    return fetch_all(url, settings)


def fetch_mr_versions(mr_iid):
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/versions")
    return fetch_all(url, settings)


def fetch_mr_version(mr_iid, version):
    url = urljoin(settings.GITLAB_URL,
                  f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/versions/{version}")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    return r.json()


def fetch_mr_patches(mr_iid):
    url = urljoin(settings.GITLAB_URL, f"/{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}.patch")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    r.encoding = 'utf-8'
    return r.text


def find_original_comment(mr_iid, note_id):
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/discussions")
    discussions = fetch_all(url, settings)
    for d in discussions:
        if 'notes' in d:
            for i in range(1, len(d['notes'])):
                if d['notes'][i]['id'] == note_id:
                    return d['notes'][i - 1]
    return None


def splitat(line):
    last = -1
    for m in re.finditer(r"[ \t]+", line):
        if m.start() > 72:
            if last < 0:
                return m.start()
            return last
        last = m.start()
    return len(line)


def quote_original_comment(note):
    ret = []
    lines = note['body'].splitlines()
    dt = dateutil.parser.parse(note['updated_at'])
    prefix = "On {}, {} wrote:\n".format(dt.strftime("%c %z"), note['author']['name'])
    for line in lines:
        while len(line) > 72:
            i = splitat(line)
            if i == len(line):
                break
            ret.append(line[0:i] + "\n")
            line = line[i+1:]
        if len(line) > 0:
            ret.append(line + "\n")
    return prefix + '> ' + '> '.join(ret)


def fetch_diffnotes(mr_iid, author, note_id, position):
    # This is a KLUDGE.  The discussions.json entry point is not documented.
    #  It provides back an array of all discussions on a given Merge request.
    #  We can use it to pick out a particular DiffNote, using the position.
    #  Human readable text seems to come up in 'truncated_diff_lines'.
    #  I've made an ask for a better way to do this, but it seems to work for now.
    url = urljoin(settings.GITLAB_URL, f"/{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}/discussions.json")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    discussions = r.json()
    diffnotes = ""
    for d in discussions:
        if d['notes'][0]['id'] == str(note_id):
            if 'truncated_diff_lines' in d and 'position' in d:
                if d['position'] == position:
                    diffnotes += "{} commented about {}:\n".format(author, d['diff_file']['new_path'])
                    if 'truncated_diff_lines' in d:
                        for diff in d['truncated_diff_lines']:
                            diffnotes += "> " + diff['text'] + "\n"
                    break
    return diffnotes


def error_in_notes(mr_iid, text):
    url = urljoin(settings.GITLAB_URL, f"/{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}/discussions.json")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    notes = r.json()
    for n in notes:
        for n2 in n['notes']:
            if n2['note'].find(text) >= 0:
                return True
    return False


def fetch_specific_mr(iid):
    url = urljoin(settings.GITLAB_URL,
                  f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{iid}")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    return r.json()


def fetch_recently_updated_mrs(after):
    after = after.strftime(GITLAB_TIME_FORMAT)
    url = urljoin(settings.GITLAB_URL,
                  f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests" +
                  f"?updated_after=f{after}&order_by=updated_at&sort=asc")
    return fetch_all(url, settings)


def fetch_project(id):
    url = urljoin(settings.GITLAB_URL,
                  f"api/v4/projects/{id}/")
    r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
    r.raise_for_status()
    return r.json()


# HELPERS


def create_message_id(mr_iid, mr_version, patch_nr=None, note_id=None, event_id=None):
    project = re.sub(r'/', '-', settings.GITLAB_PROJECT_NAME)

    if note_id:
        note_part = f"-note{note_id}"
    else:
        note_part = ""

    if patch_nr:
        patch_part = f"-patch{patch_nr}"
    else:
        patch_part = ""

    if event_id:
        event_part = f"-event{event_id}"
    else:
        event_part = ""

    return f"<{project}-mr{mr_iid}-v{mr_version}{patch_part}{note_part}{event_part}@gitlab-mail-bridge>"


def create_reference(mr_id, hostname):
    return f"<merge_request_{mr_id}@{hostname}>"


def post_note(mr_iid, note):
    url = urljoin(settings.GITLAB_URL, f"api/v4/projects/{settings.GITLAB_PROJECT_ID}/merge_requests/{mr_iid}/notes")
    r = requests.post(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN}, data={'body': note})
    r.raise_for_status()


def parse_gitlab_datetime(dt):
    return dateutil.parser.parse(re.sub('T', ' ', dt))


def decode_rfc2822(header):
    result = []
    for binary, charset in email.header.decode_header(header):
        decoded = None
        if isinstance(binary, str):
            result.append(binary)
            continue

        if charset is not None:
            try:
                decoded = binary.decode(charset, errors='ignore')
            except Exception as e:
                print("unable to decode {}: {}".format(charset, e))

        if decoded is None:
            try:
                decoded = binary.decode('utf8', errors='ignore')
            except Exception as e:
                print("unable to decode as utf8: {}".format(e))
                decoded = '0x{}'.format(binary.hex())

        result.append(decoded)

    return ''.join(result)


def split_mbox_into_messages(mbox):
    result = []
    # Ported from Git's mailsplit code to regex.
    regex = r'^(?=.{20,})(?=From .*\d{2}:\d{2}:\d{2}\s*(?:9[1-9]|\d{3,})(?!\d)[^:\n]*$)'
    for mail in re.split(regex, mbox, flags=re.M|re.A):
        if not mail:
            continue
        result.append(email.message_from_string(mail))
    return result


def get_mr_version_for_date(versions, date):
    date = parse_gitlab_datetime(date)
    for i, version in enumerate(versions):
        if parse_gitlab_datetime(version["created_at"]) < date:
            return len(versions)-i

    raise RuntimeError(f"unable to get mr version for {date}")


def get_patch_sha1(mail):
    return re.findall(r"From (\w+)", mail.get_unixfrom())[0]


# Return a positive number representing the human comprehensible
#  version number - *not* an index into the versions array.
def get_mr_version(versions, sha1):
    for i, version in enumerate(reversed(versions)):
        if version["head_commit_sha"] == sha1:
            return i+1


def get_mr_version_date(versions, version):
    v = list(reversed(versions))[version-1]
    return parse_gitlab_datetime(v['created_at'])


# PROCESS EVENTS


def process_commented_on(event):
    if event['note']['noteable_type'] != 'MergeRequest':
        log(f"unknown noteable type {event['note']['noteable_type']}")
        return

    date = event['created_at']
    title = event['target_title']
    note_id = event['note']['id']
    mr_iid = event['note']['noteable_iid']
    mr_id = event['note']['noteable_id']
    mr_versions = fetch_mr_versions(mr_iid)
    mr_version = get_mr_version_for_date(mr_versions, date)
    mr_url = f"{settings.GITLAB_URL}{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}#note_{note_id}"

    body = ""
    author = f"{event['note']['author']['name']} (@{event['note']['author']['username']})"

    # TODO - this is fairly fragile.  hoping to get guidance here:
    #          https://forum.gitlab.com/t/end-point-to-retrieve-a-diffnote/66926
    if event['note']['type'] == 'DiffNote':
        diffnote = fetch_diffnotes(event['note']['noteable_iid'], author, event['note']['id'], event['note']['position'])
        body += diffnote

    # If we are part of a thread, naively quote the last comment
    #  this seems slightly improve reported context.
    # Unless the commentor has already injected some quoting, in which
    #  case let's trust them to get it right.
    quoting = len(event['note']['body']) > 0 and event['note']['body'][0] == '>'
    original = find_original_comment(event['note']['noteable_iid'], event['note']['id'])
    if original and not quoting:
        body += quote_original_comment(original)

    body += event['note']['body']

    bot = re.search(r'project_.*_bot', event['note']['author']['username'])
    if bot:
        log("comment was originally from the mailing list.")
        return

    patches = split_mbox_into_messages(fetch_mr_patches(mr_iid))
    nr_patches = len(patches)

    mail = email.message.Message()
    mail['From'] = email.utils.formataddr((author, settings.BRIDGE_FROM_EMAIL))
    mail['Subject'] = "Re: " + cover_subject(mr_version, nr_patches, mr_iid, title)
    mail['Message-ID'] = create_message_id(mr_iid, mr_version, note_id=note_id)
    mail['In-Reply-To'] = create_message_id(mr_iid, mr_version)
    mail['References'] = create_reference(mr_id, gitlab_hostname) + " " + mail['In-Reply-To']
    mail['Date'] = email.utils.format_datetime(parse_gitlab_datetime(date))
    mail.set_payload(f"{body}\n\n-- \n{mr_url}")

    return mail


def process_status(event):
    if event['target_type'] != 'MergeRequest':
        log(f"unknown target_type {event['target_type']}, skipping")
        return

    date = event['created_at']
    title = event['target_title']
    mr_iid = event['target_iid']
    mr_id = event['target_id']
    mr_versions = fetch_mr_versions(mr_iid)
    mr_version = get_mr_version_for_date(mr_versions, date)
    mr_url = f"{settings.GITLAB_URL}{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}"
    author = f"{event['author']['name']} (@{event['author']['username']})"
    action_name = event['action_name']
    event_id = event['id']

    patches = split_mbox_into_messages(fetch_mr_patches(mr_iid))
    nr_patches = len(patches)

    mail = email.message.Message()
    mail['From'] = email.utils.formataddr((author, settings.BRIDGE_FROM_EMAIL))
    mail['Subject'] = "Re: " + cover_subject(mr_version, nr_patches, mr_iid, title) + f" - {action_name}"
    mail['Message-ID'] = create_message_id(mr_iid, mr_version, event_id=event_id)
    mail['In-Reply-To'] = create_message_id(mr_iid, mr_version)
    mail['References'] = create_reference(mr_id, gitlab_hostname) + " " + mail['In-Reply-To']
    mail['Date'] = email.utils.format_datetime(parse_gitlab_datetime(date))
    mail.set_payload(f"\nThis merge request was {action_name} by {event['author']['name']}.\n-- \n{mr_url}")

    return mail


def process_unknown(event):
    log(f"unknown event action name '{event['action_name']}'")


def process_ignore(event):
    log(f"event {event['id']} {event['action_name']} ignored")


EVENT_ACTION_TYPES = {
        "commented on": process_commented_on,
        "accepted":     process_ignore,
        "approved":     process_status,
        "closed":       process_status,
        "pushed to":    process_ignore,
        "pushed new":   process_ignore,
        "opened":       process_ignore,
        }


def process_event(event):
    log(f"Event {event['id']} {event['action_name']} - processing")
    proc = EVENT_ACTION_TYPES.get(event["action_name"], process_unknown)
    mail = proc(event)

    if mail:
        create_headers_from_event(mail, event)
        log(f"sending email {mail['Subject']}")
        send_email(mail)


# MAIN


def fixup_patch(patch, mr_iid, mr_version, patch_nr):
    payload = patch.get_payload()
    original_from = decode_rfc2822(patch['From'])
    mr_url = f"{settings.GITLAB_URL}{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}"
    patch.set_payload(f"From: {original_from}\n\n{payload}{mr_url}")
    name, _ = email.utils.parseaddr(original_from)
    del patch['From']
    patch['From'] = email.utils.formataddr((name, settings.BRIDGE_FROM_EMAIL))
    patch['Message-ID'] = create_message_id(mr_iid, mr_version, patch_nr=patch_nr)
    patch['In-Reply-To'] = create_message_id(mr_iid, mr_version, patch_nr=0)

    subject = patch['Subject']
    del patch['Subject']

    if subject.startswith('[PATCH]'):
        subject = re.sub(r'PATCH', 'PATCH 1/1', subject)

    patch['Subject'] = re.sub(r'PATCH', f'PATCH{BRIDGE_TAG_INFIX}', subject)


def fixup_date(mail, date):
    del mail['Date']
    mail['Date'] = email.utils.format_datetime(date)


def fixup_version(mail, version):
    if version <= 1:
        return
    subject = mail['Subject']
    del mail['Subject']
    mail['Subject'] = re.sub(r'PATCH ([0-9])', f"PATCH v{version} \\1", subject)


def done_by_maintainer(author, commits):
    for commit in commits:
        if commit['committer_name'].casefold() != author.casefold():
            return True
    return False


def commits_meta_changed(new, old):
    if len(old) != len(new):
        return True
    for i in range(0, len(old)):
        for f in ['author_email', 'message', 'title']:
            if old[i][f] != new[i][f]:
                return True
    return False


def get_changes(iid, version, versionid, old_versionid, mr):
    current_changes = fetch_mr_version(iid, versionid)
    old_changes = fetch_mr_version(iid, old_versionid)
    old_commits = []
    for c in old_changes['commits']:
        old_commits.append(c['id'])

    version_string = f"  v{version}: "
    vstring_len = len(version_string)

    # After a merge, the source branch is deleted, which will short
    #  circuit this logic.  We cannot do any analysis on an MR in
    #  that condition.
    if len(old_changes['diffs']) == 0:
        return None

    # We don't want to send an email notice on a rebase or other innocuous change
    #   But this gets complicated.  There is a strong sense that if an
    #   author makes a change to their commit messages or description,
    #   that warrants another email.  But if a maintainer adds a signed off
    #   or makes a similar small change at merge time, we *don't* want that.
    # So we try to figure out if this is a maintainer merge, and if so
    #   we want to more aggressively 'quiet' this logic.  So basically,
    #   if the messages are not changed, or only changed by a maintainer, let's be quiet.
    if 'commits' in current_changes and 'commits' in old_changes:
        changed = commits_meta_changed(current_changes['commits'], old_changes['commits'])
        maintainer = done_by_maintainer(mr['author']['name'], current_changes['commits'])
        if maintainer or not changed:
            if 'diffs' in current_changes and 'diffs' in old_changes:
                if current_changes['diffs'] == old_changes['diffs']:
                    return None

    if 'diffs' in current_changes:
        # ...there is no diff
        if len(current_changes['diffs']) == 0 or len(current_changes['diffs'][0]) == 0:
            return None

    changes = ""
    for c in current_changes['commits']:
        if not c['id'] in old_commits:
            if len(changes) > 0:
                changes = changes + " " * vstring_len
            changes = changes + c['title'] + "\n"
    return "\n" + version_string + changes


def cover_subject(mr_version, nr_patches, mr_iid, title):
    vstring = ""
    if mr_version > 1:
        vstring = f" v{mr_version}"
    return f"[PATCH{BRIDGE_TAG_INFIX}{vstring} 0/{nr_patches}] MR{mr_iid}: {title}"


def create_cover(mr_id, mr_iid, mr_version, versions, nr_patches, mr):
    author_name = f"{mr['author']['name']} (@{mr['author']['username']})"
    title = mr['title']
    description = mr['description']

    mail = email.message.Message()
    mail['From'] = email.utils.formataddr((author_name, settings.BRIDGE_FROM_EMAIL))
    changes = ""
    if mr_version > 1:
        if len(versions) > 1:
            # The versions array seems to be most recent first, while mr_version
            #   is the positive version number.  TODO: this assumes that the
            #   versions array is ordered, which Arek seems to not have found
            vindex = mr_version * -1
            oindex = (mr_version - 1) * -1
            changes = get_changes(mr_iid, mr_version, versions[vindex]['id'], versions[oindex]['id'], mr)
            if changes is None:
                return None

    mail['Subject'] = cover_subject(mr_version, nr_patches, mr_iid, title)
    mail['Message-ID'] = create_message_id(mr_iid, mr_version)
    mail['References'] = create_reference(mr_id, gitlab_hostname)

    mr_url = f"{settings.GITLAB_URL}{settings.GITLAB_PROJECT_NAME}/-/merge_requests/{mr_iid}"
    payload = f"{description}\n\n--{changes} \n"
    if nr_patches > settings.MAXIMUM_PATCHES:
        payload += "This merge request has too many patches to be relayed via email.\n"
        payload += "Please visit the URL below to see the contents of the merge request.\n"
    mail.set_payload(payload + mr_url)

    return mail


def process_mr(mr, update_db, assigner):
    iid = mr['iid']
    log(f"MR{iid} updated - processing")

    if mr['merged_at']:
        log(f"MR{iid} merged - skipping")
        return

    # We jump through some hoops because the fetch_mr_patches
    #  entry point on GitLab does not allow one
    #  to specify a particular version.  You can only get
    #  a nice set of patches for the most current version.
    # Out of an abundance of caution, we make sure that the
    #  last sha1 matches a known version and then we use that.
    updated_at = parse_gitlab_datetime(mr['updated_at'])
    patches = split_mbox_into_messages(fetch_mr_patches(iid))
    if not patches:
        log(f"Warning: MR {iid} doesn't contain any patch")
        if update_db:
            db.set_last_mr_updated_at(updated_at)
        return

    sha1s = [get_patch_sha1(patch) for patch in patches]
    versions = fetch_mr_versions(iid)
    version = get_mr_version(versions, sha1s[-1])
    if not version:
        # We can get an unusual case where we do not have a patch
        #  to go with the start of the versions chain.  Andrew
        #  was able to do this in wine-demo MR 13, by somehow
        #  having a commit message only commit.  Arek believes that
        #  this is not a sensible MR, and we should flag it as such.
        log(f"Error: MR {iid} patch {sha1s[-1]} not in versions")
        error = ("This merge request appears to be malformed.\n"
                 "This can be caused by a commit with no content.\n"
                 "Please check your merge request for errors.")

        if not error_in_notes(iid, error):
            post_note(iid, error)
        if update_db:
            db.set_last_mr_updated_at(updated_at)
        return

    date = get_mr_version_date(versions, version)
    if db.was_mr_version_processed(iid, version):
        log(f"MR{iid}v{version} - skipping, already processed")
        return

    if update_db:
        db.mark_mr_version_processed(iid, version)
        db.set_last_mr_updated_at(updated_at)

    nr_patches = len(patches)
    cover = create_cover(mr['id'], iid, version, versions, nr_patches, mr)
    if cover is None:
        log(f"MR{iid}v{version} - skipping, has no changes")
        return

    # Assign reviewers if there are none currently, but only on the first submission
    if len(mr['reviewers']) == 0 and len(versions) == 1:
        full_version = fetch_mr_version(iid, versions[0]['id'])
        assigner.assign_reviewers(iid, mr['author'], full_version, update_db)

    fixup_date(cover, date)
    create_headers_from_mr(cover, mr)
    send_email(cover)
    if nr_patches <= settings.MAXIMUM_PATCHES:
        for nr, mail in enumerate(patches):
            # The hope is that a slight delay will allow receivers to order the patches correctly.
            time.sleep(0.1)
            if mail['From'] == None:
                log(f"Error: MR {iid} patch {nr} has no From")
                continue

            fixup_patch(mail, iid, version, nr+1)
            date = date + datetime.timedelta(seconds=1)
            fixup_date(mail, date)
            fixup_version(mail, version)
            create_headers_from_mr(mail, mr)
            mail['References'] = create_reference(mr['id'], gitlab_hostname) + " " + mail['In-Reply-To']
            log(f"MR{iid}v{version} - sent email")
            send_email(mail)


def handle_debug_requests(assigner):
    for arg in sys.argv[2:]:
        if arg.find("mr=") == 0:
            print(f"Processing MR iid {arg[3:]}")
            mr = fetch_specific_mr(int(arg[3:]))
            process_mr(mr, False, assigner)
        elif arg.find("event=") == 0:
            print(f"Processing event id {arg[6:]}")
            # I did not immediately see a way to get a specific event.
            # This is debug code, so I'm at peace with doing this inefficiently
            date = (datetime.datetime.now() - datetime.timedelta(days=365)).date()
            for event in fetch_events(date):
                if event['id'] == int(arg[6:]):
                    process_event(event)
        else:
            print(f"Error: unknown argument {arg}", file=sys.stderr)


def main():

    assigner = Assign(settings)

    if len(sys.argv) > 2:
        handle_debug_requests(assigner)
        return

    # Process any new merge requests
    last_mr_updated_at = db.get_last_mr_updated_at()
    if not last_mr_updated_at:
        last_mr_updated_at = datetime.datetime.now() - datetime.timedelta(days=settings.INITIAL_BACKLOG_DAYS)
        if not settings.READ_ONLY:
            db.set_last_mr_updated_at(last_mr_updated_at)

    for mr in fetch_recently_updated_mrs(last_mr_updated_at):
        process_mr(mr, not settings.READ_ONLY, assigner)

    date = db.get_last_event_date()
    if not date:
        date = (datetime.datetime.now() - datetime.timedelta(days=settings.INITIAL_BACKLOG_DAYS)).date()
        if not settings.READ_ONLY:
            db.set_last_event_date(date)

    date = date - datetime.timedelta(days=1)
    last_event_id = db.get_last_event_id()
    if not last_event_id:
        last_event_id = 0

    for event in fetch_events(date):
        if event['id'] <= last_event_id:
            continue
        process_event(event)

        if not settings.READ_ONLY:
            db.set_last_event_date(parse_gitlab_datetime(event['created_at']).date())
            db.set_last_event_id(event['id'])


if __name__ == "__main__":
    main()
