#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# This script performs janitorial tasks. It removes incomplete patch series,
# purges older jobs and patches, etc.
#
# Copyright 2009 Ge van Geldorp
# Copyright 2017 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}
my $Name0 = $0;
$Name0 =~ s+^.*/++;

use File::Path;

use WineTestBot::Config;
use WineTestBot::Failures;
use WineTestBot::Jobs;
use WineTestBot::Log;
use WineTestBot::Patches;
use WineTestBot::PendingPatchSets;
use WineTestBot::CGI::Sessions;
use WineTestBot::RecordGroups;
use WineTestBot::Tasks;
use WineTestBot::Users;
use WineTestBot::Utils;
use WineTestBot::VMs;


#
# Logging and error handling helpers
#

my $LogOnly;
sub Trace(@)
{
  print @_ if (!$LogOnly);
  LogMsg @_;
}

my @Errors;
sub Error(@)
{
  print STDERR "$Name0:error: ", @_ if (!$LogOnly);
  LogMsg @_;
  push @Errors, "error: ". join("", @_);
}


#
# Setup and command line processing
#

# Grab the command line options
my ($Usage, $DryRun);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--dry-run")
  {
    $DryRun = 1;
  }
  elsif ($Arg eq "--log-only")
  {
    $LogOnly = 1;
  }
  elsif ($Arg =~ /^(?:-\?|-h|--help)$/)
  {
    $Usage = 0;
    last;
  }
  else
  {
    Error "unexpected argument '$Arg'\n";
    $Usage = 2;
    last;
  }
}
# Check parameters
if (defined $Usage)
{
  print "Usage: $Name0 [--dry-run] [--log-only] [--help]\n";
  exit $Usage;
}


#
# Main
#

sub DeletePath($)
{
  my ($Path) = @_;

  Trace "Deleting '$Path'\n";
  if (!$DryRun and !rmtree($Path))
  {
    Error "Could not delete '$Path': $!\n";
  }
}

# Delete obsolete Jobs
if ($JobPurgeDays != 0)
{
  my $Jobs = CreateJobs();
  $Jobs->AddFilter("Submitted", [time() - $JobPurgeDays * 86400], "<");
  foreach my $Job (@{$Jobs->GetItems()})
  {
    Trace "Deleting job ", $Job->Id, "\n";
    next if ($DryRun);

    $Job->RmTree();
    my $ErrMessage = $Jobs->DeleteItem($Job);
    Error "$ErrMessage\n" if (defined $ErrMessage);
  }
}

# Delete PatchSets that are more than a day old
my $DeleteBefore = time() - 1 * 86400;
my $Sets = CreatePendingPatchSets();
foreach my $Set (@{$Sets->GetItems()})
{
  my ($MostRecentPatch, @Parts);
  foreach my $Part (@{$Set->Parts->GetItems()})
  {
    push @Parts, $Part->No;
    my $Patch = $Part->Patch;
    if (! defined($MostRecentPatch) ||
        $MostRecentPatch->Received < $Patch->Received)
    {
      $MostRecentPatch = $Patch;
    }
  }
  if (! defined($MostRecentPatch) ||
      $MostRecentPatch->Received < $DeleteBefore)
  {
    my $Version = $Set->Version ? " v". $Set->Version : "";
    Trace "Deleting pending series$Version for ", $Set->EMail, " (got ", join(" ", sort @Parts), " / ", $Set->TotalParts, ")\n";
    next if ($DryRun);

    $Sets->DeleteItem($Set);
    $MostRecentPatch->Disposition("Incomplete series, discarded");
    $MostRecentPatch->Save();
  }
}

# Delete obsolete Patches now that no Job references them
if ($JobPurgeDays != 0)
{
  my $Patches = CreatePatches();
  $Patches->AddFilter("Received", [time() - $JobPurgeDays * 86400], "<");
  foreach my $Patch (@{$Patches->GetItems()})
  {
    my $Jobs = CreateJobs();
    $Jobs->AddFilter("Patch", [$Patch]);
    if ($Jobs->IsEmpty())
    {
      Trace "Deleting patch ", $Patch->Id, "\n";
      next if ($DryRun);

      unlink("$DataDir/patches/" . $Patch->Id);
      my $ErrMessage = $Patches->DeleteItem($Patch);
      Error "$ErrMessage\n" if (defined $ErrMessage);
    }
  }
}

# And also failures marked for deletion
my $Failures = CreateFailures();
$Failures->AddFilter("BugStatus", ["deleted"]);
foreach my $Failure (@{$Failures->GetItems()})
{
  if ($Failure->TaskFailures->IsEmpty())
  {
    $Failures->DeleteItem($Failure);
  }
  else
  {
    my $TaskFailure = @{$Failure->TaskFailures->GetItems()}[0];
    Trace "Keeping failure ", $Failure->Id, " for task log ", join("/", $TaskFailure->JobId, $TaskFailure->StepNo, $TaskFailure->TaskNo, $TaskFailure->TaskLog), "\n";
  }
}

# Purge the deleted VMs if they are not referenced anymore
my $VMs = CreateVMs();
$VMs->AddFilter("Role", ["deleted"]);
my %DeletedVMs;
map { $DeletedVMs{$_} = 1 } @{$VMs->GetKeys()};

if (%DeletedVMs)
{
  my $Tasks = CreateTasks($VMs);
  $Tasks->AddFilter("VMName", [keys %DeletedVMs]);
  foreach my $Task (@{$Tasks->GetItems()})
  {
    next if (!exists $DeletedVMs{$Task->VMName});
    Trace "Keeping the ", $Task->VMName, " VM for task ", $Task->GetSlashKey(), "\n";
    delete $DeletedVMs{$Task->VMName};
    last if (!%DeletedVMs);
  }
  foreach my $VMKey (keys %DeletedVMs)
  {
    Trace "Deleting the $VMKey VM\n";
    next if ($DryRun);

    my $VM = $VMs->GetItem($VMKey);
    my $ErrMessage = $VMs->DeleteItem($VM);
    if (defined $ErrMessage)
    {
      Error "Unable to delete the $VMKey VM: $ErrMessage\n";
    }
  }
}

# Purge the deleted users if they are not referenced anymore
my $Users = CreateUsers();
$Users->AddFilter("Status", ["deleted"]);
my %DeletedUsers;
map { $DeletedUsers{$_} = 1 } @{$Users->GetKeys()};

if (%DeletedUsers)
{
  foreach my $Job (@{CreateJobs($Users)->GetItems()})
  {
    if (exists $DeletedUsers{$Job->User->Name})
    {
      Trace "Keeping the ", $Job->User->Name, " account for job ", $Job->Id, "\n";
      delete $DeletedUsers{$Job->User->Name};
      last if (!%DeletedUsers);
    }
  }

  foreach my $UserKey (keys %DeletedUsers)
  {
    Trace "Deleting the $UserKey account\n";
    next if ($DryRun);

    my $User = $Users->GetItem($UserKey);
    DeleteSessions($User);
    my $ErrMessage = $Users->DeleteItem($User);
    if (defined $ErrMessage)
    {
      Error "Unable to delete the $UserKey account: $ErrMessage\n";
    }
  }
}

# Check the content of the staging directory
if (opendir(my $dh, "$DataDir/staging"))
{
  # We will be deleting files so read the directory in one go
  my @Entries = readdir($dh);
  close($dh);
  foreach my $Entry (@Entries)
  {
    next if ($Entry eq "." or $Entry eq "..");

    $Entry =~ m%^([^/]+)$%; # untaint
    my $FileName = "$DataDir/staging/$1";
    my $Age = int(-M $FileName);
    my $TTL = $JobPurgeDays ? $JobPurgeDays - $Age : undef;

    # FIXME: There are still some obsolete unexpired 31 characters session ids.
    if ($Entry =~ /^[0-9a-f]{31,32}-websubmit2?_/)
    {
      # We get these files whenever a developer abandons a job submission.
      # So just delete them silently after a day.
      $TTL = 1 - $Age;
    }
    elsif ($Entry =~ /^[0-9a-f]{32}_(?:patch\.diff|patchset\.diff|email|winetest(?:64)?-latest\.(?:exe|url)|work)$/)
    {
      # Janitor can only see these files if it ran during the brief interval
      # between their creation and them being moved out of staging, or if
      # something prevented the latter (power loss, bug, etc). So only complain
      # after a day to avoid false positives.
      if ($TTL >= 1)
      {
        my $Deletion = defined $TTL ? " (deletion in $TTL days)" : "";
        Error "Found an old transient file$Deletion: staging/$Entry\n"
      }
    }
    else
    {
      my $Deletion = defined $TTL ? " (deletion in $TTL days)" : "";
      Error "Found a suspicious file$Deletion: staging/$Entry\n";
    }

    DeletePath($FileName) if (defined $TTL and $TTL <= 0);
  }
}
else
{
  Error "Unable to open '$DataDir/staging': $!";
}

# Check the content of the latest directory
if (opendir(my $dh, "$DataDir/latest"))
{
  # We will be deleting files so read the directory in one go
  my @Entries = readdir($dh);
  close($dh);

  my $AllVMs = $VMs->Clone();
  foreach my $Entry (@Entries)
  {
    next if ($Entry eq "." or $Entry eq "..");
    # Needed to analyze Wine patches
    next if ($Entry eq "winefiles.txt" or $Entry eq "wine-parentsrc.txt");
    # Needed to update Windows VMs
    next if ($Entry =~ /^TestAgentd(?:32|64)\.exe$/);
    # Needed to run the tests
    next if ($Entry =~ /^TestLauncher(?:32|64)\.exe$/);
    next if ($Entry =~ /^winetest(?:64)?-latest\.(?:exe|url)$/);

    $Entry =~ m%^([^/]+)$%; # untaint
    my $FileName = "$DataDir/latest/$1";
    my $Age = int(-M $FileName);
    my $TTL = $JobPurgeDays ? $JobPurgeDays - $Age : undef;

    # Keep the regexp in sync with WineTestBot::Task::GetRefReportName()
    if ($Entry !~ /^[a-zA-Z0-9_]+-job[0-9.]+-[a-zA-Z0-9_]+\.(?:log|report)(?:\.errors)?$/)
    {
      my $Deletion = defined $TTL ? " (deletion in $TTL days)" : "";
      Error "Found a suspicious file$Deletion: latest/$Entry\n";
    }

    DeletePath($FileName) if (defined $TTL and $TTL <= 0);
  }
}
else
{
  Error "Unable to open '$DataDir/latest': $!";
}

# Delete obsolete record groups
if ($JobPurgeDays != 0)
{
  my $RecordGroups = CreateRecordGroups();
  $RecordGroups->AddFilter("Timestamp", [time() - $JobPurgeDays * 86400], "<");
  foreach my $RecordGroup (@{$RecordGroups->GetItems()})
  {
    if ($DryRun)
    {
      Trace "Deleting RecordGroup ", $RecordGroup->Id, "\n";
    }
    else
    {
      my $ErrMessage = $RecordGroups->DeleteItem($RecordGroup);
      Error "$ErrMessage\n" if (defined $ErrMessage);
    }
  }
}

if (@Errors)
{
  NotifyAdministrator("Errors occurred during the TestBot cleanup",
                      "Some errors occurred while cleaning up the TestBot database and directories:\n\n".
                      join("", @Errors));
}
