USE winetestbot;

ALTER TABLE Steps
  ADD LogTime ENUM('Y', 'N') NULL DEFAULT 'N'
      AFTER DebugLevel;

ALTER TABLE Steps
  ALTER LogTime DROP DEFAULT;
