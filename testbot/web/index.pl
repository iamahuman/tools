# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# WineTestBot status page
#
# Copyright 2009 Ge van Geldorp
# Copyright 2013-2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package JobStatusBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use ObjectModel::BasicPropertyDescriptor;
use ObjectModel::CGI::ValueFormatter;

use WineTestBot::Branches;
use WineTestBot::Users;
use WineTestBot::Utils;

my $DAYS_DEFAULT = 4;


sub GetPropertyDescriptors($)
{
  my ($self) = @_;

  # Add some extra columns
  my @PropertyDescriptors;
  foreach my $PropertyDescriptor (@{$self->{Collection}->GetPropertyDescriptors()})
  {
    my $PropertyName = $PropertyDescriptor->GetName();
    next if ($PropertyName =~ /^(?:Branch|User|PatchId|Patch)$/);
    next if ($PropertyName eq "BranchName" and !CreateBranches()->MultipleBranchesPresent);
    if ($PropertyName eq "Status")
    {
      push @PropertyDescriptors, CreateBasicPropertyDescriptor("Warnings", "Warnings", !1, !1, "N", 6);
    }
    push @PropertyDescriptors, $PropertyDescriptor;
  }

  return \@PropertyDescriptors;
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName eq "Submitted" ? ("ro", "timetipdate") :
         $self->SUPER::DisplayProperty($PropertyDescriptor);
}

sub GenerateHeaderView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "Priority")
  {
    print "<a class='title' title='Higher values indicate a lower priority'>Nice</a>";
  }
  elsif ($PropertyName eq "Status")
  {
    print "<a class='title' title='Status - New / All failures'>Status</a>";
  }
  elsif ($PropertyName eq "Warnings")
  {
    print "<a class='title' title='New / All warnings'>Warnings</a>";
  }
  elsif ($PropertyName eq "Submitted")
  {
    print "<a class='title' title='Date'>Submitted</a>";
  }
  elsif ($PropertyName eq "Ended")
  {
    print "<a class='title' title='Completion timestamp'>Elapsed</a>";
  }
  else
  {
    $self->SUPER::GenerateHeaderView($Row, $Col);
  }
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $Job = $Row->{Item};
  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "UserName")
  {
    if (defined $Job->PatchId and defined $Job->Patch->FromName and
        $Job->UserName eq GetBatchUser()->Name)
    {
      print $self->escapeHTML($Job->Patch->FromName);
    }
    else
    {
      print $self->escapeHTML($Job->UserName);
    }
  }
  elsif ($PropertyName eq "Warnings")
  {
    my $JobInfo = $self->{JobsInfo}->{$Job->Id};
    if (!exists $JobInfo->{Warnings})
    {
      print "&nbsp;";
    }
    else
    {
      my $DetailsLink = $self->GetDetailsLink($Row);
      print "<a id='job", $Job->Id, "' href='", $self->escapeHTML($DetailsLink), "'>";
      if (exists $JobInfo->{NewWarnings})
      {
        my $class = $JobInfo->{NewWarnings} ? "testfail" : "success";
        print "<span class='$class'>$JobInfo->{NewWarnings}</span> / ";
      }
      my $class = $JobInfo->{Warnings} ? "testwarn" : "success";
      print "<span class='$class'>$JobInfo->{Warnings}</span></a>";
    }
  }
  elsif ($PropertyName eq "Status")
  {
    my $DetailsLink = $self->GetDetailsLink($Row);
    print "<a id='job", $Job->Id, "' href='", $self->escapeHTML($DetailsLink),
          "'>";

    my %HTMLChunks = ("queued" => "<span class='queued'>queued</span>",
                      "running" => "<span class='running'>running</span>",
                      "completed" => "<span class='success'>completed</span>",
                      "badpatch" => "<span class='badpatch'>bad patch</span>",
                      "badbuild" => "<span class='badbuild'>build error</span>",
                      "boterror" => "<span class='boterror'>TestBot error</span>",
                      "canceled" => "<span class='canceled'>canceled</span>",
        );
    my $Status = $Job->Status;
    my $HTMLStatus = $HTMLChunks{$Status} || $Status;
    if ($Status eq "completed" || $Status eq "running" || $Status eq "boterror" || $Status eq "canceled")
    {
      my $JobInfo = $self->{JobsInfo}->{$Job->Id};

      my $FailuresPrefix;
      if ($Status eq "completed")
      {
        $FailuresPrefix = "";
      }
      elsif ($Status ne "running")
      {
        print $HTMLStatus;
        $FailuresPrefix = " - ";
      }
      elsif ($JobInfo->{IsRunning})
      {
        $JobInfo->{DoneTasks} ||= 0;
        print "<span class='running'>running <small>($JobInfo->{DoneTasks}/$JobInfo->{TaskCount})</small></a>";
        $FailuresPrefix = " - ";
      }
      else
      {
        $JobInfo->{DoneTasks} ||= 0;
        print "<span class='running'>queued <small>($JobInfo->{DoneTasks}/$JobInfo->{TaskCount})</small>";
        $FailuresPrefix = " - ";
      }

      if (exists $JobInfo->{TestFailures})
      {
        print $FailuresPrefix;
        if (defined $JobInfo->{NewTestFailures})
        {
          my $class = $JobInfo->{NewTestFailures} ? "testfail" : "success";
          print "<span class='$class'>$JobInfo->{NewTestFailures}</span> / ";
        }
        my $class = $JobInfo->{TestFailures} ? "testfail" : "success";
        print "<span class='$class'>$JobInfo->{TestFailures}</span>";
      }
      elsif ($Status eq "completed")
      {
        print $HTMLStatus;
      }
    }
    else
    {
      print $HTMLStatus;
    }
    print "</a>";
  }
  elsif ($PropertyName eq "Ended")
  {
    if (defined $Job->Ended)
    {
      my $Duration = $Job->Ended - $Job->Submitted;
      GenerateTipDateTime($Job->Ended, DurationToString($Duration));
    }
    else
    {
      print "&nbsp;";
    }
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}


package VMStatusBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);


sub GetDetailsPage($)
{
  my ($self) = @_;
  my $Session = $self->{EnclosingPage}->GetCurrentSession();
  my $CurrentUser = $Session->User if (defined $Session);
  return (!$CurrentUser or !$CurrentUser->HasRole("admin")) ? undef :
         "/admin/VMDetails.pl";
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName =~ /^(?:Name|Type|Role|Status|Description)$/ ? "ro" :
         "";
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "Description")
  {
    my $VM = $Row->{Item};
    print "<details><summary>",
          $self->escapeHTML($VM->Description || $VM->Name),
          "</summary>", $self->escapeHTML($VM->Details || "No details!"),
          "</details>";
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}

package StatusPage;

use ObjectModel::CGI::FreeFormPage;
our @ISA = qw(ObjectModel::CGI::FreeFormPage);

use ObjectModel::BasicPropertyDescriptor;
use ObjectModel::Collection;

use WineTestBot::Config;
use WineTestBot::Engine::Notify;
use WineTestBot::Jobs;
use WineTestBot::Log;
use WineTestBot::Tasks;
use WineTestBot::VMs;


sub _initialize($$$)
{
  my ($self, $Request, $RequiredRole) = @_;

  $self->{start} = Time();
  my @PropertyDescriptors = (
    CreateBasicPropertyDescriptor("Days", "Days ", !1, 1, "N", 2),
  );
  $self->SUPER::_initialize($Request, $RequiredRole, \@PropertyDescriptors);

  if (!$self->GetParam("Days") or !$self->Validate() or
      !int($self->GetParam("Days"))) # 00 case!
  {
    # Replace with the default value so the cutoff can be calculated
    $self->SetParam("Days", $DAYS_DEFAULT);
  }
  if (!$self->GetErrMessage() and $self->GetParam("Days") <= $DAYS_DEFAULT)
  {
    $self->SetRefreshInterval(60);
  }
  # The action is a no-op so unset it to not need to redefine OnAction()
  $self->SetParam("Action", undef);
}

sub OutputDot($$)
{
  my ($self, $DotColor) = @_;

  print "<img src='/images/${DotColor}dot.jpg' alt='${DotColor} dot' " .
        "width='20' height='20' />";
}

sub GenerateBody($)
{
  my ($self) = @_;

  print "<h1>${ProjectName} Test Bot status</h1>\n";
  print "<div class='Content'>\n";

  print "<h2>General</h2>\n";
  print "<div class='GeneralStatus'>\n";
  print "<div class='GeneralStatusItem'>";
  if (PingEngine())
  {
    $self->OutputDot("green");
    print "<div class='GeneralStatusItemText'><a href='#jobs'>Engine is alive and processing jobs</a></div>";
  }
  else
  {
    $self->OutputDot("red");
    print "<div class='GeneralStatusItemText'><a href='#jobs'>Engine appears to be dead and is not processing jobs</a></div>";
  }
  print "</div>\n";

  my $VMs = CreateVMs();
  my ($OfflineVMs, $MaintenanceVMs);
  foreach my $VM (@{$VMs->GetItems()})
  {
    $OfflineVMs++ if ($VM->Status eq "offline");
    $MaintenanceVMs++ if ($VM->Status eq "maintenance");
  }
  if (!$OfflineVMs and !$MaintenanceVMs)
  {
    print "<div class='GeneralStatusItem'>";
    $self->OutputDot("green");
    print "<div class='GeneralStatusItemText'><a href='#vms'>All VMs are online</a></div>";
    print "</div>\n";
  }
  else
  {
    if ($OfflineVMs)
    {
      print "<div class='GeneralStatusItem'>";
      $self->OutputDot("red");
      print "<div class='GeneralStatusItemText'><a href='#vms'>$OfflineVMs VMs are offline</a></div>";
      print "</div>\n";
    }
    if ($MaintenanceVMs)
    {
      print "<div class='GeneralStatusItem'>";
      $self->OutputDot("red");
      print "<div class='GeneralStatusItemText'><a href='#vms'>$MaintenanceVMs VMs are undergoing maintenance</a></div>";
      print "</div>\n";
    }
  }
  print "</div>\n";

  print "<h2><a name='jobs'></a>Jobs</h2>\n";
  my $Jobs = CreateJobs();
  my $Days = $self->GetParam("Days");
  my $CutOff = time() - $Days * 24 * 60 * 60;
  $Jobs->AddFilter("Submitted", [$CutOff], ">=");
  my $JobsCollectionBlock = new JobStatusBlock($Jobs, $self);
  $JobsCollectionBlock->SetReadWrite(0);

  # We need to collect information about the tasks of all jobs except the
  # pretty rare queued jobs. But doing so one job at a time is inefficient so
  # do it all at once now and store the results in ...->{JobsInfo}.
  my $Tasks = CreateTasks();
  $Tasks->AddFilter(FilterOr(FilterValue("Started", ">=", [$CutOff]),
                             FilterNull("Started")));
  foreach my $Task (@{$Tasks->GetItems()})
  {
    my $JobInfo = ($JobsCollectionBlock->{JobsInfo}->{$Task->JobId} ||= {});
    $JobInfo->{TaskCount}++;
    if ($Task->Status eq "running")
    {
      $JobInfo->{IsRunning} = 1;
    }
    elsif ($Task->Status ne "queued")
    {
      $JobInfo->{DoneTasks}++;
    }

    foreach my $Counter ("TestFailures", "NewTestFailures", "Warnings", "NewWarnings")
    {
      my $Value = $Task->$Counter;
      $JobInfo->{$Counter} += $Value if (defined $Value);
    }
  }

  $JobsCollectionBlock->GenerateList();

  print <<EOF;
<p></p>
<form id="Days" action='/' method='get'>
  <div class='ItemProperty'><label>Show jobs for the past</label>
    <div class='ItemValue'>
      <input type='text' name='Days' maxlength='2' size='2' value='$Days' /> days
      <input type='submit' value='Update'/>
    </div>
  </div>
</form>
EOF

  print "<h2><a name='vms'></a>VMs</h2>\n";
  my $VMsCollectionBlock = new VMStatusBlock($VMs, $self);
  $VMsCollectionBlock->SetReadWrite(0);
  $VMsCollectionBlock->GenerateList();
  print "</div>\n";

  print "<p class='GeneralFooterText'>Generated in ", Elapsed($self->{start}), " s</p>\n";
}


package main;

my $Request = shift;
my $Page = StatusPage->new($Request, "");
$Page->GeneratePage();
