# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Known failures index page
#
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package FailuresBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use WineTestBot::Config;
use WineTestBot::Failures;


sub _initialize($$$)
{
  my ($self, $Collection, $EnclosingPage) = @_;

  $self->SUPER::_initialize($Collection, $EnclosingPage);

  my $Session = $EnclosingPage->GetCurrentSession();
  if (!$Session or !$Session->User->HasRole("admin"))
  {
    $self->SetReadWrite(0);
    # and hide the deleted entries
    $self->{Collection}->AddFilter("BugStatus", ["deleted"], "<>");
  }
}

sub Create($$)
{
  my ($Collection, $EnclosingPage) = @_;

  return FailuresBlock->new($Collection, $EnclosingPage);
}

sub GenerateFormStart()
{
  my ($self) = @_;
  $self->SUPER::GenerateFormStart();

  my $Config = $self->escapeHTML($self->{EnclosingPage}->GetParam("Config") || "");
  my $Module = $self->escapeHTML($self->{EnclosingPage}->GetParam("Module") || "");
  my $Line = $self->escapeHTML($self->{EnclosingPage}->GetParam("Line") || "");
  print <<EOF;
  <div class='ItemProperty'><label><a class='title' title="Optionally add the architecture after a ':'">Test configuration</a></label>
    <div class='ItemValue'>
      <input type='text' name='Config' placeholder='VM name' maxlength='20' size='20' value='$Config' />
    </div>
  </div>
  <div class='ItemProperty'><label><a class='title' title='or "Report validation errors"'>Module</a></label>
    <div class='ItemValue'>
      <input type='text' name='Module' placeholder='Dll or program name'  maxlength='64' size='64' value='$Module' />
    </div>
  </div>
  <div class='ItemProperty'><label>Failure line</label>
    <div class='ItemValue'>
      <input type='text' name='Line' placeholder='Failure, unhandled exception, validation error, ...' maxlength='512' size='64' value='$Line' />
    </div>
  </div>
  <div class='ItemActions'>
    <input type='submit' value='Filter'/>
  </div>
<p></p>
EOF
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName =~ /^(?:Notes|ConfigRegExp|FailureRegExp)$/ ? "" :
         $PropertyName =~ /^Last(?:Old|New)$/ ? ("ro", "datetiptime") :
         $self->SUPER::DisplayProperty($PropertyDescriptor);
}

sub GenerateHeaderView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "ErrorGroup")
  {
    print "<a class='title' title='or Error group'>Test module</a>";
  }
  else
  {
    $self->SUPER::GenerateHeaderView($Row, $Col);
  }
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  my $PropertyName = $Col->{Descriptor}->GetName();
  if ($PropertyName eq "TestUnit")
  {
    print "<a href='", $self->escapeHTML($self->GetDetailsLink($Row)), "'>",
          $self->escapeHTML($Row->{Item}->$PropertyName), "</a>";
  }
  elsif ($PropertyName eq "BugStatus")
  {
    my $Status = $Row->{Item}->BugStatus || "Not fetched yet...";
    my $Class = ($Status eq "Does not exist") ? "-missing" :
                ($Status =~ /^(?:CLOSED|RESOLVED)/) ? "-closed" : "";
    print "<span class='bug$Class'>", $self->escapeHTML($Status), "</span>";
  }
  elsif ($PropertyName eq "BugId")
  {
    print "<a href='$WineBugUrl", $Row->{Item}->BugId, "' target='_blank'>",
          $self->escapeHTML($Row->{Item}->BugId), "</a>";
  }
  elsif ($PropertyName eq "BugDescription")
  {
    my $Value = $Row->{Item}->BugDescription || "Not fetched yet...";
    my $Tooltip ="";
    if ($Row->{Item}->Notes)
    {
      $Tooltip = " title='". $self->escapeHTML($Row->{Item}->Notes) ."'";
    }
    print "<a href='", $self->escapeHTML($self->GetDetailsLink($Row)),
          "'$Tooltip>", $self->escapeHTML($Value), "</a>";
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}

sub GetSortedItems($)
{
    my ($self) = @_;
    my $Items = $self->{Collection}->GetSortedItems();

    my $Config = $self->{EnclosingPage}->GetParam("Config");
    my $Module = $self->{EnclosingPage}->GetParam("Module");
    my $Line = $self->{EnclosingPage}->GetParam("Line");
    if ($Config or $Module or $Line)
    {
      my $TestUnit;
      $TestUnit = $1 if ($Line and $Line =~ /^([_a-z0-9]+)\.c:\d+:/);

      my @Failures;
      foreach my $Failure (@$Items)
      {
        next if ($Module and $Module ne $Failure->ErrorGroup);
        next if ($TestUnit and $TestUnit ne $Failure->TestUnit);

        if ($Config)
        {
          my $RegExp = $Failure->ConfigRegExp;
          my $Match = eval {
            use warnings FATAL => qw(regexp);
            !$RegExp or $Config =~ /$RegExp/;
          };
          next if (!$Match);
        }
        if ($Line)
        {
          my $RegExp = $Failure->FailureRegExp;
          my $Match = eval {
            use warnings FATAL => qw(regexp);
            !$RegExp or $Line =~ /$RegExp/;
          };
          next if (!$Match);
        }

        push @Failures, $Failure;
      }
      $Items = \@Failures;
    }
    return $Items;
}

sub GetItemActions($)
{
  my ($self) = @_;

  return ["Delete", "Restore"];
}

sub OnItemAction($$$)
{
  my ($self, $Failure, $Action) = @_;

  if ($self->{RW})
  {
    return 1 if ($Action eq "Restore" and $Failure->BugStatus ne "deleted");

    my $NewStatus = $Action eq "Delete" ? "deleted" :
                    $Action eq "Restore" ? "" :
                    undef;
    if (defined $NewStatus)
    {
      $Failure->BugStatus($NewStatus);
      my ($_ErrProperty, $ErrMessage) = $Failure->Save();
      if (defined $ErrMessage)
      {
        # Setting the error field is only useful on form pages
        $self->{EnclosingPage}->AddError($ErrMessage);
        return 0;
      }
      return 1;
    }
  }

  return $self->SUPER::OnItemAction($Failure, $Action);
}


package main;

use ObjectModel::CGI::CollectionPage;
use WineTestBot::Failures;

my $Request = shift;
my $Page = ObjectModel::CGI::CollectionPage->new($Request, "", CreateFailures(), \&FailuresBlock::Create);
$Page->GeneratePage();
